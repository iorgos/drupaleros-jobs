<?php

namespace Drupal\jobs\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Job edit forms.
 *
 * @ingroup jobs
 */
class JobForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Se agregó el empleo: %label.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Se guardó el empleo: %label.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.job.canonical', ['job' => $entity->id()]);
  }

}
