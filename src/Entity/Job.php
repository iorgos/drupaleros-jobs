<?php

namespace Drupal\jobs\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\jobs\JobInterface;

/**
 * Defines the job entity class.
 *
 * @ContentEntityType(
 *   id = "job",
 *   label = @Translation("Empleo"),
 *   label_collection = @Translation("Empleos"),
 *   label_singular = @Translation("empleo"),
 *   label_plural = @Translation("empleos"),
 *   label_count = @PluralTranslation(
 *     singular = "@count empleo",
 *     plural = "@count empleos",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\jobs\JobListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\jobs\Form\JobForm",
 *       "edit" = "Drupal\jobs\Form\JobForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\jobs\JobHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\jobs\JobAccessControlHandler",
 *   },
 *   base_table = "job",
 *   admin_permission = "administer jobs",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/job",
 *     "add-form" = "/job/add",
 *     "canonical" = "/job/{job}",
 *     "edit-form" = "/job/{job}/edit",
 *     "delete-form" = "/job/{job}/delete",
 *   },
 *   field_ui_base_route = "entity.job.settings",
 * )
 */
class Job extends ContentEntityBase implements JobInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type); // Add id, uuid

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
        // Del archivo: core.entity_form_display.job.job.default.yml
        // label:
        //   type: string_textfield
        //   weight: 0
        //   region: content
        //   settings:
        //     size: 60
        //     placeholder: ''
        //   third_party_settings: {  }
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
        // Del archivo: core.entity_view_display.job.job.default.yml
        // label:
        //   type: string
        //   label: hidden
        //   settings:
        //     link_to_entity: false
        //   third_party_settings: {  }
        //   weight: 0
        //   region: content
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the job was created.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
