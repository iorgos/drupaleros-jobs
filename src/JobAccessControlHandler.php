<?php

namespace Drupal\jobs;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Access controller for the Job entity.
 *
 * @see \Drupal\jobs\Entity\Job.
 */
class JobAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        /** @var \Drupal\jobs\JobInterface $entity */
        if ($entity->get('status')->value) {
          return AccessResult::allowed();
        }
      case 'update':
      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'administer jobs');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'administer jobs');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    /** @var \Drupal\jobs\JobInterface $entity */
    $entity = $items ? $items->getEntity() : NULL;
    if ($entity && 'description' == $field_definition->getName() && 'view' == $operation) {
      if ($account->hasPermission('administer jobs')) {
        return AccessResult::allowed()->cachePerPermissions();
      }
      if ($entity && !$entity->get('status')->value) {
        return AccessResult::forbidden()->addCacheableDependency($entity);
      }
    }
    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }


}
